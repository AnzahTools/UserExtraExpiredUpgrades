<?php


namespace AnzahTools\UserExtraExpiredUpgrades\XF\Admin\Controller;


use XF\Mvc\ParameterBag;

class User extends XFCP_User
{
	
	public function actionExtra(ParameterBag $params)
	{
		$user = $this->assertUserExists($params->user_id);
		$response = parent::actionExtra($params);
		$upgradeRepo = $this->repository('XF:UserUpgrade');
		$upgrades = $upgradeRepo->findExpiredUserUpgradesForList()->where('user_id', $user->user_id)->fetch();
		$response->setParam('expiredUpgrades', $upgrades);
		return $response;
	}
	
	
}